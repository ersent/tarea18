#include <stdio.h>
#include "menu.h"
int main(int argc, char const *argv[]) {

  int seleccion=1;
  int dep=0;
  int ret=0;
  int saldo=0;

  while (seleccion) {
    seleccion=menu();
    switch (seleccion) {
  case 1:
      printf("Escribe la cantidad a depositar: $ ");
      scanf("%d", &dep);
      printf("Su nuevo saldo es: $%d\n", saldo+dep);
        break;
  case 2:
      printf("Escribe la cantidad a retirar: $ ");
      scanf("%d", &ret);
      if (ret>saldo) {
        printf("ERROR: La cantidad a retirar no puede ser mayor al saldo\n");
      }else{
        printf("Su nuevo saldo es: $%d\n", saldo-ret);
      }
        break;
  case 3:
        printf("Usted a donado $15 a Bécalos, muchas gracias.\n");
        printf("Su nuevo saldo es: $%d\n", saldo-15);
        break;
  case 4:
        printf("Su saldo actual es: $%d\n", saldo);
        break;
    case 0:
        printf("Saliendo...\n");
        break;
      default:
        printf("Opción inválida\n");
    }
  }
  return 0;
}
